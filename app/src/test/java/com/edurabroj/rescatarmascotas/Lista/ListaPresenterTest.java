package com.edurabroj.rescatarmascotas.Lista;

import com.edurabroj.rescatarmascotas.Entidades.Mascota;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

public class ListaPresenterTest {
    private ListaContract.View view;
    private ListaContract.Interactor interactor;
    private ListaPresenter presenter;

    @Before
    public void setUp() throws Exception {
        //Se mockean las interfaces y se inicializa el presenter
        view = Mockito.mock(ListaContract.View.class);
        interactor = Mockito.mock(ListaContract.Interactor.class);
        presenter = new ListaPresenter(view, interactor);
    }

    @Test
    public void seLlamaACargarDatosDeInteractor() {
        /*
        * Se verifica que al llamar onInitialDataLoad en el presenter,
        * el interactor llame al método obtenerDatos
        * */
        presenter.onInitialDataLoad();
        Mockito.verify(interactor).obtenerDatos(presenter);
    }

    @Test
    public void cuandoRecibeDatosOcultaProgress() {
        /*
         * Se verifica que al recibir los datos
         * se oculte el cargando
         * */
        presenter.onSuccess(new ArrayList<Mascota>());
        Mockito.verify(view).hideCargando();
    }

    @Test
    public void cuandoRecibeDatosLosMeteEnAdaptador() {
        presenter.onSuccess(new ArrayList<Mascota>());
        Mockito.verify(view).setData(new ArrayList<Mascota>());
    }

    @Test
    public void cuandoFallaPeticionSetteaError() {
        presenter.onFailed();
        Mockito.verify(view).showError();
    }
}