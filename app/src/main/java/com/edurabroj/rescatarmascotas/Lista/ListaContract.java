package com.edurabroj.rescatarmascotas.Lista;

import com.edurabroj.rescatarmascotas.Entidades.Mascota;

import java.util.List;

//El contrato define las interfaces para View, Presenter e Interactor
public class ListaContract {
    /*Manejo de views, cambios en la ui*/
    public interface View {
        void showCargando();
        void hideCargando();
        void setData(List<Mascota> data);
        void showError();
    }

    //eventos a manejar
    //comunicación entre view e interactor
    public interface Presenter {
        void onInitialDataLoad();
        void onReloadButtonClick();
    }

    //Obtención de datos
    public interface Interactor {
        void obtenerDatos(OnDataFinishedListener listener);

        //Escuchador para la obtención de datos
        interface OnDataFinishedListener {
            void onSuccess(List<Mascota> data);
            void onFailed();
        }
    }

}
