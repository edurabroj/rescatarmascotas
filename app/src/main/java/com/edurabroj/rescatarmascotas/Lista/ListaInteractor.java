package com.edurabroj.rescatarmascotas.Lista;

import android.os.Handler;

import com.edurabroj.rescatarmascotas.Entidades.Mascota;

import java.util.ArrayList;

public class ListaInteractor implements ListaContract.Interactor {
    @Override
    public void obtenerDatos(final OnDataFinishedListener listener) {
        //falsa obtención de datos
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                listener.onSuccess(new ArrayList<Mascota>(){{
                    add(new Mascota(){{
                        setTitle("Yonson");
                    }});
                    add(new Mascota(){{
                        setTitle("Pitavo");
                    }});
                    add(new Mascota(){{
                        setTitle("Javinto");
                    }});
                }});
            }
        }, 2000);
    }
}
