package com.edurabroj.rescatarmascotas.Lista;

import com.edurabroj.rescatarmascotas.Entidades.Mascota;

import java.util.List;

//El presentador implementa la interfaz del Presenter y la inrerfaz del listener del Interactor
public class ListaPresenter implements ListaContract.Presenter, ListaContract.Interactor.OnDataFinishedListener {
    ListaContract.View view;
    ListaContract.Interactor interactor;

    public ListaPresenter(ListaContract.View view, ListaContract.Interactor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    //Métodos de la interfaz Presenter
    @Override
    public void onInitialDataLoad() {
        view.showCargando();
        //Se pide la carga de datos al interactor
        interactor.obtenerDatos(this);
    }

    @Override
    public void onReloadButtonClick() {
        view.showCargando();
        //Se pide la carga de datos al interactor
        interactor.obtenerDatos(this);
    }

    //Métodos de la interfaz del listener
    @Override
    public void onSuccess(List<Mascota> data) {
        //Los datos obtenidos por el interactor se envian a la vista y se quita el cargando
        view.hideCargando();
        view.setData(data);
    }

    @Override
    public void onFailed() {
        //El interactor no pudo obtener los datos, se quita el cargando y se muestra el error
        view.hideCargando();
        view.showError();
    }
}
