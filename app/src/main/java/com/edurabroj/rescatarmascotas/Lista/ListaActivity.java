package com.edurabroj.rescatarmascotas.Lista;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.edurabroj.rescatarmascotas.Entidades.Mascota;
import com.edurabroj.rescatarmascotas.R;

import java.util.List;

//La activity/fragment implementa la interfaz de la vista
public class ListaActivity extends AppCompatActivity implements ListaContract.View{
    Button btnReload;
    ProgressBar progressBar;
    RecyclerView recyclerView;

    ListaContract.Presenter presenter;

    ListaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnReload = findViewById(R.id.btnReload);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerView);

        presenter = new ListaPresenter(this, new ListaInteractor());

        adapter = new ListaAdapter(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        //evento de cargar los datos iniciales
        presenter.onInitialDataLoad();

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //evento de refrescar
                presenter.onReloadButtonClick();
            }
        });
    }

    //metodos de la interfaz, que seran llamados desde el presenter para actualizar la ui
    @Override
    public void showCargando() {
        progressBar.setVisibility(View.VISIBLE);
        btnReload.setVisibility(View.GONE);
    }

    @Override
    public void hideCargando() {
        progressBar.setVisibility(View.GONE);
        btnReload.setVisibility(View.VISIBLE);
    }

    @Override
    public void setData(List<Mascota> data) {
        adapter.setDataset(data);
    }

    @Override
    public void showError() {
        Toast.makeText(this,"Error en la solicitud",Toast.LENGTH_SHORT).show();
    }
}
